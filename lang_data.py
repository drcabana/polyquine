babashka = [
[   "(let [data [",
    " ]", ],
[   " q (char 34), space (char 32), separator (char sep)]",
    " (doseq [k (range a b)]           (println (nth data k)))",
    " (doseq [k (range (count data))] (println (str space q (nth data k) q separator)))",
    " (doseq [k (range b c)]           (println (nth data k))))", ] ]

fsharp = [
[   "let data = [",
    "]", ],
[   "let q, space, separator = string(char 34), string(char 32), string(char sep)",
    "for i in a..b-1 do",
    "   System.Console.WriteLine(data.[i])",
    "for i in 0..data.Length-1 do",
    "   System.Console.WriteLine(space + q + data.[i] + q + separator)",
    "for i in b..c-1 do",
    "   System.Console.WriteLine(data.[i])", ] ]

java = [
[   "public class quine {",
    " public static void main(String[] args) {",
    "  String[] data = {",
    "  };", ],
[   "  char q = (char) 34; char space = (char) 32; char separator = (char) sep;",
    "  for (int i = a; i < b; i++)",
    "    System.out.println(data[i]);",
    "  for (int i = 0; i < data.length; i++)",
    "    System.out.println(String.valueOf(space) + q + data[i] + q + separator);",
    "  for (int i = b; i < c; i++)",
    "    System.out.println(data[i]);",
    " }",
    "}", ] ]

lua = [
[   "data = {",
    "}", ],
[   "q, space, separator = string.char(34), string.char(32), string.char(sep)",
    "for k = a+1,b do",
    "  print(data[k])",
    "end",
    "for k = 1,#data do",
    "  print(space .. q .. data[k] .. q .. separator)",
    "end",
    "for k = b+1,c do",
    "  print(data[k])",
    "end", ] ]

python = [
[   "data = [",
    "]", ],
[   "q, space, separator = chr(34), chr(32), chr(sep)",
    "for k in range(a,b):",
    "  print(data[k])",
    "for d in data:",
    "  print(space + q + d + q + separator)",
    "for k in range(b,c):",
    "  print(data[k])", ] ]

ruby = [
[   "data = [",
    "]", ],
[   "q, space, separator = 34.chr, 32.chr, sep.chr",
    "for k in a..b-1",
    "  puts data[k]",
    "end",
    "data.each { |d| puts space + q + d + q + separator}",
    "for k in b..c-1",
    "  puts data[k]",
    "end", ] ]

facts = { "babashka" :{"boundary":1 , "template":babashka , "separator":32 ,
                       "size": 1 + len(babashka[0]) + len(babashka[1]),
                       "parms": " [a b c sep] [{} {} {} {}]"},

         "fsharp"  :{"boundary":1 , "template":fsharp ,  "separator":59 ,
                     "size": 1 + len(fsharp[0]) + len(fsharp[1]),
                     "parms": "let a, b, c, sep = {}, {}, {}, {}"},

         "java"    :{"boundary":3  ,"template":java ,  "separator":44 ,
                     "size": 1 + len(java[0]) + len(java[1]),
                     "parms": "  int a={}; int b={}; int c={}; int sep={};"},

         "lua"     :{"boundary":1 , "template":lua ,  "separator":44 ,
                     "size": 1 + len(lua[0]) + len(lua[1]),
                     "parms": "a, b, c, sep= {}, {}, {}, {}"},

         "python"  :{"boundary":1 , "template":python,  "separator":44 ,
                     "size": 1 + len(python[0]) + len(python[1]),
                     "parms": "a, b, c, sep = {}, {}, {}, {}"},

         "ruby"    :{"boundary":1 , "template":ruby,  "separator":44 ,
                     "size": 1 + len(ruby[0]) + len(ruby[1]),
                     "parms": "a, b, c, sep = {}, {}, {}, {}" } }

supported_langs = facts.keys()

def boundary(lang):
    assert lang in supported_langs
    return facts[lang]["boundary"]

def template(lang):
    assert lang in supported_langs
    return facts[lang]["template"]

def parameters(lang):
    assert lang in supported_langs
    return facts[lang]["parms"]

def separator(lang):
    assert lang in supported_langs
    return facts[lang]["separator"]

def size(lang):
    assert lang in supported_langs
    return facts[lang]["size"]